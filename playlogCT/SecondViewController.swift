//
//  SecondViewController.swift
//  playlogCT
//
//  Created by Akhlaq Rao on 3/26/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import UIKit
import SpriteKit

import Photos

extension PLCTScene {
    
    func setupSceneParameters() {
        self.lapsDuration = 7
        self.repeatCount = 1
        self.startAngle = 180
        self.endAngle = -180.5
    }
    
    func addBackground() {
        let background = SKSpriteNode(imageNamed: "black-grill")
        background.position = CGPoint(x: frame.midX, y: frame.midY)
        background.texture?.filteringMode = .nearest
        background.name = "background"
        background.aspectFillToSize(fillSize: frame.size) // Do this after you set texture
        addChild(background)
    }
    
    func addRandomNumberLabel() {
        let randomNumberLabel = SKLabelNode(text: "0")
        randomNumberLabel.fontSize = 36;
        randomNumberLabel.horizontalAlignmentMode = .center
        randomNumberLabel.verticalAlignmentMode = .center
        randomNumberLabel.fontColor = SKColor.white
        randomNumberLabel.fontName = "AvenirNext-Bold"
        randomNumberLabel.name = "randomNumberLabel"
        randomNumberLabel.position = CGPoint(x: frame.midX, y: frame.midY)
        addChild(randomNumberLabel)
    }
    
    func updateRandomNumberLabel(randomNumber:Int) {
        let randomNumberLabel = self.scene?.childNode(withName: "randomNumberLabel") as! SKLabelNode
        randomNumberLabel.text = String(randomNumber)
    }
    
    func updateBackground(image: UIImage) {
        let background = self.scene?.childNode(withName: "background") as! SKSpriteNode
        background.texture = SKTexture(image: image)
    }
}

class SecondViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PLCTSceneDelegate {
    
    //Array of PHAsset type for storing photos
    var images = [PHAsset]()
    var segment:Double = 0
    var scene:PLCTScene!
    var randomNumber:Int!
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scene = PLCTScene(size: view.frame.size)
        scene.plctsceneDelegate = self
        scene.addBackground()
        scene.addRandomNumberLabel()
        let skView = view as! SKView
        skView.presentScene(scene)
        
        randomNumber = randomNumberGenerator()
        
        self.photosCollectionView.delegate = self
        self.photosCollectionView.allowsSelection = true
        self.getImages()
        
        let asset = self.images[0]
        let manager = PHImageManager.default()
        
        _ = Int(manager.requestImage(for: asset,
                                     targetSize: CGSize(width: 120.0, height: 120.0),
                                     contentMode: .aspectFill,
                                     options: nil) { (result, _) in
                                        self.scene.updateBackground(image: result!)
                                                })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.scene.setupSceneParameters()
    }
    
    func randomNumberGenerator() -> Int {
        return Int.random(in: 10 ... 20)
    }
    
    func getImages() {
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        assets.enumerateObjects({ (object, count, stop) in
            // self.cameraAssets.add(object)
            self.images.append(object)
        })
        
        //In order to get latest image first, we just reverse the array
        self.images.reverse()
        let first22 = self.images.prefix(22)
        self.images = Array(first22)
        // To show photos, I have taken a UICollectionView
        self.photosCollectionView.reloadData()
    }
    
    func anglePLCTSceneDelegate(angle: Double) {
        if angle.rounded() > 0.0 {
            if segment == angle.rounded() {
                
                if Int(segment/Double(360/randomNumber)) < randomNumber  {
                    print(Int(segment/Double(360/randomNumber)))
                    
                    let asset = self.images[Int(segment/Double(360/randomNumber))]
                    let manager = PHImageManager.default()
                    
                    _ = Int(manager.requestImage(for: asset,
                                                 targetSize: CGSize(width: 120.0, height: 120.0),
                                                 contentMode: .aspectFill,
                                                 options: nil) { (result, _) in
                                                    self.scene.updateBackground(image: result!)
                    })
                    segment = segment + Double(360/randomNumber)
                }
            }
        }
        else {
            segment = Double(360/randomNumber)
        }
    }
    
    func didTapPLCTSceneDelegate() {
        randomNumber = randomNumberGenerator()
        self.scene.updateRandomNumberLabel(randomNumber: randomNumber)
        segment = Double(360/randomNumber)
        self.images.shuffle()
        
        let asset = self.images[0]
        let manager = PHImageManager.default()
        
        _ = Int(manager.requestImage(for: asset,
                                     targetSize: CGSize(width: 120.0, height: 120.0),
                                     contentMode: .aspectFill,
                                     options: nil) { (result, _) in
                                        self.scene.updateBackground(image: result!)
        })
        
        self.photosCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return randomNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        let asset = images[indexPath.row]
        let manager = PHImageManager.default()
        
        if cell.tag != 0 {
            manager.cancelImageRequest(PHImageRequestID(cell.tag))
        }
        
        cell.tag = Int(manager.requestImage(for: asset,
                                            targetSize: CGSize(width: 120.0, height: 120.0),
                                            contentMode: .aspectFill,
                                            options: nil) { (result, _) in
                                                cell.photoImageView?.image = result
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width * 0.20
        let height = self.view.frame.height * 0.179910045
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let asset = images[indexPath.row]
        let manager = PHImageManager.default()
        
        _ = Int(manager.requestImage(for: asset,
                                     targetSize: CGSize(width: 120.0, height: 120.0),
                                     contentMode: .aspectFill,
                                     options: nil) { (result, _) in
                                    self.scene.updateBackground(image: result!)
        })
    }
}

