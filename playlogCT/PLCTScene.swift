//
//  PLCTScene.swift
//  playlogCT
//
//  Created by Akhlaq Rao on 3/26/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import Foundation
import SpriteKit

extension Double {
    func toRadians() -> CGFloat {
        //return self * CGFloat(Double.pi) / 180.0
        return CGFloat(self) * CGFloat(Double.pi) / 180.0
    }
}

extension CGPoint {
    func distance(point: CGPoint) -> CGFloat {
        return abs(CGFloat(hypotf(Float(point.x - x), Float(point.y - y))))
    }
    
    func CGPointDistanceSquared(from: CGPoint, to: CGPoint) -> CGFloat {
        return (from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)
    }
    
    func CGPointDistance(point: CGPoint) -> CGFloat {
        return sqrt(CGPointDistanceSquared(from: point, to: CGPoint(x: x, y: y)))
    }
}

extension SKSpriteNode {
    
    func aspectFillToSize(fillSize: CGSize) {
        
        if texture != nil {
            self.size = texture!.size()
            
            let verticalRatio = fillSize.height / self.texture!.size().height
            let horizontalRatio = fillSize.width /  self.texture!.size().width
            
            let scaleRatio = horizontalRatio > verticalRatio ? horizontalRatio : verticalRatio
            
            self.setScale(scaleRatio)
        }
    }
    
}

protocol PLCTSceneDelegate {
    func didTapPLCTSceneDelegate()
    func anglePLCTSceneDelegate(angle:Double)
}

class PLCTScene: SKScene, SKSceneDelegate {
    let inputRange:CGFloat = 10
    var startAngle:Double = 181
    var endAngle:Double = -181.5
    let outerCircleRadius:CGFloat = 125
    let label = SKLabelNode(text: "90")
    let circle = SKShapeNode(circleOfRadius: 25 ) // Size of Circle
    let innerCircle = SKShapeNode(circleOfRadius: 90 ) // Size of Circle
    let outerCircle = SKShapeNode(circleOfRadius: 125 )
    var plctsceneDelegate:PLCTSceneDelegate?
    var circleTouch: UITouch?
    var lapsDuration:Double = 3
    var repeatCount:Int = 3
    
    override func didMove(to view: SKView) {
        
        self.scene!.delegate = self
        
        outerCircle.path = UIBezierPath(arcCenter: CGPoint(x: frame.midX, y: frame.midY), radius: outerCircleRadius, startAngle: startAngle.toRadians(), endAngle: endAngle.toRadians(), clockwise: false).cgPath
        outerCircle.strokeColor = .clear
        outerCircle.lineWidth = 8
        addChild(outerCircle)
        
        innerCircle.position = CGPoint(x: frame.midX, y: frame.midY)  //Middle of Screen
        innerCircle.strokeColor = SKColor(red: 63/255, green: 0, blue: 255/255, alpha: 1.0)
        innerCircle.lineWidth = 8
        addChild(innerCircle)
        
        let angleEarth: Double = 361
        let angleEarthAfterCalculate: CGFloat = CGFloat(angleEarth*Double.pi/361) - CGFloat(Double.pi/361)
        let earthX = frame.midX + cos(angleEarthAfterCalculate)*outerCircleRadius
        let earthY = frame.midY + sin(angleEarthAfterCalculate)*outerCircleRadius
        
        circle.position = CGPoint(x: earthX, y: earthY)  //Middle of Screen
        circle.strokeColor = SKColor.black
        circle.glowWidth = 1.0
        circle.fillColor = SKColor.orange
        circle.name = "circle"
        addChild(circle)
        
        label.fontSize = 14;
        label.horizontalAlignmentMode = .center
        label.verticalAlignmentMode = .center
        label.fontColor = SKColor.black
        label.fontName = "Avenir"
        label.name = "label"
        circle.addChild(label)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap))
        view.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func tap(recognizer: UIGestureRecognizer) {
        let viewLocation = recognizer.location(in: view)
        let sceneLocation = convertPoint(fromView: viewLocation)
        let touchedNode = self.nodes(at: sceneLocation)
        
        if touchedNode.count > 0 {
            if (touchedNode.contains(circle)) {
                outerCircle.path = UIBezierPath(arcCenter: CGPoint(x: frame.midX, y: frame.midY), radius: outerCircleRadius, startAngle: startAngle.toRadians(), endAngle: endAngle.toRadians(), clockwise: false).cgPath
                let circularMove = SKAction.follow(outerCircle.path!, asOffset: false, orientToPath: false, duration: self.lapsDuration)
                print(self.lapsDuration)
                let circularMoveRepeat = SKAction.repeat(circularMove, count: self.repeatCount)
                print(self.repeatCount)
                circle.run(circularMoveRepeat)
                
                if let delg = self.plctsceneDelegate {
                    delg.didTapPLCTSceneDelegate()
                }
            }
        }
    }
    
    func getAngle(a:CGPoint, b:CGPoint) -> Double {
        let x = a.x
        let y = a.y
        let dx = b.x - x
        let dy = b.y - y
        
        let radians = atan2(dy,dx); // in radians
        let degrees = radians * 180 / 3.14 // in degrees
        
        if (degrees < 0) {
            return Double(fabsf(Float(degrees)));
        }else{
            return Double(360 - degrees);
        }
    }
    
    //Mark: SKScene
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            if atPoint(touch.location(in: self)).name == "circle" || atPoint(touch.location(in: self)).name == "label" {
                circleTouch = touch
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            if circleTouch != nil {
                if touch as UITouch == circleTouch! {
                    let location = touch.location(in: self)
                    
                    let dist = location.CGPointDistance(point: CGPoint(x: frame.midX, y: frame.midY))
                    
                    if (Int(dist) >= Int(outerCircleRadius - inputRange) && Int(dist) <= Int(outerCircleRadius + inputRange)) {
                        let theta = atan2(location.x-frame.midX, location.y-frame.midY)
                        let newX = sin(theta) * outerCircleRadius
                        let newY = cos(theta) * outerCircleRadius
                        
                        let moveToAction = SKAction.move(to: CGPoint(x: frame.midX + CGFloat(newX), y: frame.midY + CGFloat(newY)), duration: 0.1)
                        moveToAction.timingMode = .easeOut // This line is optional
                        circle.run(moveToAction)
                    }
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            if circleTouch != nil {
                if touch as UITouch == circleTouch! {
                    circleTouch = nil
                }
            }
        }
    }
    
    //Mark: SKSceneDelegate
    func update(_ currentTime: TimeInterval, for scene: SKScene) {
        //print("update")
    }
    
    func didEvaluateActions(for scene: SKScene) {
        //print("didEvaluateActions")
    }
    
    func didSimulatePhysics(for scene: SKScene) {
        guard let circleNode = scene.childNode(withName: "circle") as? SKShapeNode else {
            return
        }
        guard let circleLabel = circleNode.childNode(withName: "label") as? SKLabelNode else {
            return
        }
        
        let angle = getAngle(a: circleNode.position, b: CGPoint(x: frame.midX, y: frame.midY))
        circleLabel.text = NSString(format:"%@%@", Int(angle.rounded()).description, "\u{00B0}") as String
        
        if let delg = self.plctsceneDelegate {
            delg.anglePLCTSceneDelegate(angle: angle)
        }
    }
}

