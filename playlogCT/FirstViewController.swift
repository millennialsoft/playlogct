//
//  FirstViewController.swift
//  playlogCT
//
//  Created by Akhlaq Rao on 3/26/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import UIKit
import SpriteKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = PLCTScene(size: view.frame.size)
        let skView = view as! SKView
        skView.presentScene(scene)
    }
}

