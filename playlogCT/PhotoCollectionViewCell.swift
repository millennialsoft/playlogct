//
//  PhotoCollectionViewCell.swift
//  playlogCT
//
//  Created by Akhlaq Rao on 3/26/19.
//  Copyright © 2019 MillennialSoft. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
}
